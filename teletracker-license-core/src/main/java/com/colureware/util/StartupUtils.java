package com.colureware.util;

/**
 * 
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;

import com.colureware.configuration.Configuration;
import com.colureware.configuration.impl.ConfigurationFactoryImpl;
import com.colureware.spring.ConfigurationPropertySourceWrapper;

/**
 * @author yazan
 *
 */
public class StartupUtils {

    public static final String GLOBAL_CONFIG_DIR_SYS_PROP = "global-config-dir";

    private static boolean exit = false;

    private static final Logger LOGGER = LoggerFactory.getLogger(StartupUtils.class);

    /**
     * @param contextLocation
     * @return
     */
    public static ClassPathXmlApplicationContext startClasspathXMLApplicationContext(String contextLocation) {
	try {
	    ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext();
	    configeLoggerFile();
	    Configuration configuration = new ConfigurationFactoryImpl().createConfiguration();
	    ConfigurationPropertySourceWrapper configurationPropertySource = new ConfigurationPropertySourceWrapper(
		    "application-properties", configuration);
	    applicationContext.getEnvironment().getPropertySources().addFirst(configurationPropertySource);
	    applicationContext.setConfigLocation(contextLocation);
	    applicationContext.refresh();
	    return applicationContext;
	} catch (Exception exception) {
	    // Don't remove this, its important for tracing excelsior errors
	    exception.printStackTrace();
	    LOGGER.error("Faield to start Application", exception);
	    throw new RuntimeException("Faield to start Application", exception);
	}
    }
    
    public static ClassPathXmlApplicationContext startSingleInstanceClasspathXMLApplicationContext(String contextLocation) {
	checkIfAlreadyRunning();
	return startClasspathXMLApplicationContext(contextLocation);
    }

    /**
     * 
     */
    private static void checkIfAlreadyRunning() {
	// TODO Auto-generated method stub
    }

    /**
     * TODO: remove if not necessary
     * 
     */
    private static void configeLoggerFile() {
	String loggerConigurationFileLocation = System.getProperty(GLOBAL_CONFIG_DIR_SYS_PROP);
	String loggerConfigurationFileFullPath = "log.xml";
	if (loggerConigurationFileLocation != null) {
	    loggerConfigurationFileFullPath = loggerConigurationFileLocation + "/" + loggerConfigurationFileFullPath;
	}
	LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();

	try {
	    JoranConfigurator configurator = new JoranConfigurator();
	    configurator.setContext(context);
	    context.reset();
	    configurator.doConfigure(loggerConfigurationFileFullPath);
	} catch (JoranException je) {
	    throw new RuntimeException(je.getMessage(), je);
	}
    }

    /**
     * 
     */
    public static void waitInfinatly() {
	while (!exit) {
	    try {
		synchronized (StartupUtils.class) {
		    StartupUtils.class.wait();
		}
	    } catch (InterruptedException e) {
	    }
	}
    }

    /**
     * 
     */
    public static void exit() {
	synchronized (StartupUtils.class) {
	    exit = true;
	    StartupUtils.class.notifyAll();
	}
    }
}
