/**
 * 
 */
package com.telelogx.teletracker.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.colureware.license.PlatformInfo;
import com.colureware.util.StartupUtils;

/**
 * @author Mohammad
 *
 */
public class Startup {

	private final static Logger LOGGER = LoggerFactory.getLogger(String.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = StartupUtils
				.startClasspathXMLApplicationContext("spring/app-context.xml");
		TeletrackerLicenseManager licenseManager = context
				.getBean(TeletrackerLicenseManager.class);
		PlatformInfo platformInfo = context.getBean(PlatformInfo.class);
		try {
			System.out.println(System.getProperty("java.home"));
			LOGGER.info("Server UID : " + platformInfo.getPlatformUniqueID());
			licenseManager.install();
			TeleTrackerLicenses license = licenseManager.verfyLecinces();
			LOGGER.info(String.format("License installed successfully (%1$s)",
					license.getAgentNumber()));
			StartupUtils.waitInfinatly();
		} catch (Exception e) {
			LOGGER.error("Failed loading license", e);
		}
	}
}
