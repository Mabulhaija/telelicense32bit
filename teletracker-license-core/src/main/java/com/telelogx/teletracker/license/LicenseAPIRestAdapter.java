package com.telelogx.teletracker.license;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mohammad Qasem
 *
 */
public class LicenseAPIRestAdapter extends AbstractHandler {

	private static final int NUMBER_OF_SALT_CAHRS = 5;
	private static final String ID_ATTRIBUTE = "id";
	private static final String LIST_OPERATION = "lst";
	private static final String AUTHENTICATE_OPERATION = "auth";
	private static final String REMOVE_OPERATION = "rmv";
	private static final String OPERATION_PARAMETER = "op";
	private static final String ADD_OPERATION = "add";

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private LicenseAPI licenseAPI;

	/**
     * 
     */
	@Override
	public void handle(String target, Request baseRequest,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		prepareResponse(request, response, baseRequest);
		String operation = request.getParameter(OPERATION_PARAMETER);
		switch (operation) {
		case ADD_OPERATION:
			addAgentID(response, request);
			break;
		case REMOVE_OPERATION:
			removeAgentID(response, request);
			break;
		case AUTHENTICATE_OPERATION:
			authenticateClient(request, response);
			break;
		case LIST_OPERATION:
			listAgents(response, request);
			break;
		default:
			logger.warn("wrong operation!");
			response.setStatus(400);

		}
	}

	/**
	 * @param response
	 * @param request
	 * @throws IOException
	 * @throws SQLException
	 */
	protected void addAgentID(HttpServletResponse response,
			HttpServletRequest request) {
		processAgentApiCall(new ApiCallProcessor() {
			@Override
			public void process(String agentID, HttpServletResponse response,
					HttpServletRequest request) {
				response.setStatus(licenseAPI.addAgentID(agentID));
			}
		}, response, request, ADD_OPERATION);
	}

	/**
	 * @param response
	 * @param request
	 */
	protected void removeAgentID(HttpServletResponse response,
			HttpServletRequest request) {
		processAgentApiCall(new ApiCallProcessor() {
			@Override
			public void process(String agentID, HttpServletResponse response,
					HttpServletRequest request) {

				response.setStatus(licenseAPI.removeAgent(agentID));

			}
		}, response, request, REMOVE_OPERATION);
	}

	/**
	 * @param request
	 * @param response
	 * @throws SQLException
	 * @throws IOException
	 */
	protected void authenticateClient(HttpServletRequest request,
			HttpServletResponse response) {
		processAgentApiCall(new ApiCallProcessor() {
			@Override
			public void process(String saltedAgent,
					HttpServletResponse response, HttpServletRequest request)
					throws IOException {
				String salt = saltedAgent.substring(saltedAgent.length()
						- NUMBER_OF_SALT_CAHRS);
				String agentID = saltedAgent.substring(0, saltedAgent.length()
						- NUMBER_OF_SALT_CAHRS);
				String mixedDummyString = salt + "a28mPxx";
				response.getWriter().print(
						licenseAPI.authorizationChecker(agentID, saltedAgent,
								mixedDummyString));

			}
		}, response, request, AUTHENTICATE_OPERATION);

	}

	/**
	 * @param response
	 * @param request
	 * @throws IOException
	 */
	protected void listAgents(HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		processApiCall(new ApiCallProcessor() {
			@Override
			public void process(String agentID, HttpServletResponse response,
					HttpServletRequest request) throws IOException {
				if (licenseAPI.listAgents() != null) {
					response.setStatus(200);
					response.getWriter().println(
							"<h2>" + licenseAPI.listAgents() + "</h2>");
				} else {
					response.setStatus(204);
				}

			}
		}, response, request, LIST_OPERATION);

	}

	/**
	 * @param processor
	 * @param response
	 * @param request
	 * @param operation
	 */
	protected void processApiCall(ApiCallProcessor processor,
			HttpServletResponse response, HttpServletRequest request,
			String operation) {
		try {
			processor.process(null, response, request);
		} catch (Exception e) {
			String agentID = request.getParameter(ID_ATTRIBUTE);
			if (agentID == null) {
				agentID = "";
				logger.warn("ID_Attribute not set!");
			}
			licenseAPI.traceOperation(operation, agentID, "Failed",
					"Error while add to DB.");
			logger.error("Error While Adding To DataBase", e);
		}
	}

	/**
	 * @param processor
	 * @param response
	 * @param request
	 * @param operation
	 */
	protected void processAgentApiCall(ApiCallProcessor processor,
			HttpServletResponse response, HttpServletRequest request,
			String operation) {
		String agent = request.getParameter(ID_ATTRIBUTE);
		if (agent != null) {
			processApiCall(new ApiCallProcessor() {
				@Override
				public void process(String agentID,
						HttpServletResponse response, HttpServletRequest request)
						throws Exception {
					processor.process(agent, response, request);
				}
			}, response, request, operation);
		} else {
			handleMissingAgentID(response, request, agent);
		}
	}

	/**
	 * @param response
	 * @param request
	 * @param agentID
	 * @throws IOException
	 */
	protected void handleMissingAgentID(HttpServletResponse response,
			HttpServletRequest request, String agentID) {
		try {
			licenseAPI.traceOperation(
					request.getParameter(OPERATION_PARAMETER), "", "Failed",
					" AgentID didnt set.");
			logger.error("id argument is Null");
			response.setStatus(412);
		} catch (Exception e) {
			logger.error("Error writing failed response");
		}
	}

	/**
	 * @param request
	 * @param response
	 * @param baseRequest
	 */
	private void prepareResponse(HttpServletRequest request,
			HttpServletResponse response, Request baseRequest) {
		response.setContentType("text/html;charset=utf-8");
		baseRequest.setHandled(true);

	}

	/**
	 * @return the licenseAPI
	 */
	public LicenseAPI getLicenseAPI() {
		return licenseAPI;
	}

	/**
	 * @param licenseAPI
	 *            the licenseAPI to set
	 */
	public void setLicenseAPI(LicenseAPI licenseAPI) {
		this.licenseAPI = licenseAPI;
	}

	/**
	 * @author Mohammad Qasim
	 *
	 */
	private interface ApiCallProcessor {

		void process(String agentID, HttpServletResponse response,
				HttpServletRequest request) throws Exception;

	}
}
