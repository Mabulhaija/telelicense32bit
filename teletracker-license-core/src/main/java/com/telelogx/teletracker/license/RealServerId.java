package com.telelogx.teletracker.license;

import org.springframework.stereotype.Component;

import com.colureware.license.impl.WindowsOSInfoImpl;

/**
 * @author yazan
 * 
 */
@Component
public class RealServerId {

	public static void main(String[] args) {
		System.out.println(new WindowsOSInfoImpl().getPlatformUniqueID());
	}
}
