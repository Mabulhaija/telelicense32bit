package com.telelogx.teletracker.license;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.sql.DataSource;

import com.google.gwt.dev.util.collect.HashSet;

/**
 * @author Mohammad Qasim
 *
 */
public class TeletrackerLicenseDAO {

	private static final String DELETE_AGENT = "DELETE FROM agents WHERE AgentID = ?";
	private static final String CHECK_AGENT_EXISTS = "SELECT AgentID FROM agents WHERE AgentID = ?";
	private static final String INSERT_NEW_AGENT = "INSERT INTO agents (AgentID) VALUES(?)";
	private static final String SELECT_ALL_AGENTS = "SELECT * FROM agents";
	private static final String INSERT_INTO_TRACE = "INSERT INTO trace_operations VALUES(?,?,?,?,?,?,?)";

	private DataSource dataSource;

	/**
     * 
     */
	public TeletrackerLicenseDAO(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	/**
	 * @param response
	 * @return
	 */
	public Set<String> listAgents() {

		Set<String> agentIds = new HashSet<String>();
		PreparedStatement pstmt = null;
		Connection connection = null;
		ResultSet sqlresult = null;
		try {
			connection = dataSource.getConnection();
			pstmt = connection.prepareStatement(SELECT_ALL_AGENTS);
			sqlresult = pstmt.executeQuery();

			while (sqlresult.next()) {
				agentIds.add(sqlresult.getString(1));
			}
		} catch (SQLException se) {
			throw new RuntimeException();
		} finally {
			if (sqlresult != null) {
				close(sqlresult);
			}
			if (pstmt != null) {
				close(pstmt);
			}
			if (connection != null) {
				close(connection);
			}
		}
		return agentIds;

	}

	/**
     * 
     */
	protected <E> E executePreparedStatement(
			PreparedStatementTemplate<E> template) {
		PreparedStatement pstmt = null;
		Connection connection = null;
		int sqlresult;
		try {
			connection = dataSource.getConnection();

			pstmt = template.prepareStatement(connection);
			sqlresult = pstmt.executeUpdate();
			E result = template.prepareResult(sqlresult);
			return result;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (pstmt != null) {
				close(pstmt);
			}
			if (connection != null) {
				close(connection);
			}
		}
	}

	/**
	 * @param response
	 * @param agnt
	 */
	public void addAgent(String agent) {
		executePreparedStatement(new PreparedStatementTemplate<Object>() {
			@Override
			public PreparedStatement prepareStatement(Connection connection)
					throws SQLException {
				PreparedStatement pstmt = connection
						.prepareStatement(INSERT_NEW_AGENT);
				pstmt.setString(1, agent);

				return pstmt;
			}

			@Override
			public Object prepareResult(int resultSet) throws SQLException {
				return null;
			}
		});
	}

	/**
	 * @param operation
	 * @param agent
	 * @param status
	 * @param desc
	 */
	public void traceOperation(String operation, String agent, String status,
			String desc) {

		executePreparedStatement(new PreparedStatementTemplate<Object>() {
			@Override
			public PreparedStatement prepareStatement(Connection connection)
					throws SQLException {
				Date currentDate = new Date();
				SimpleDateFormat timeFormatter = new SimpleDateFormat(
						"HH:mm:ss");
				SimpleDateFormat dateFormatter = new SimpleDateFormat(
						"yyyy-MM-dd");
				InetAddress ip = null;
				try {
					ip = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {

				}
				String ipAddress = ip.toString();
				String operationDate = dateFormatter.format(currentDate);
				String operationTime = timeFormatter.format(currentDate);
				PreparedStatement pstmt = connection
						.prepareStatement(INSERT_INTO_TRACE);
				pstmt.setString(1, operation);
				pstmt.setString(2, agent);
				pstmt.setString(3, ipAddress);
				pstmt.setString(4, operationDate);
				pstmt.setString(5, operationTime);
				pstmt.setString(6, status);
				pstmt.setString(7, desc);
				return pstmt;
			}

			@Override
			public Object prepareResult(int resultSet) throws SQLException {
				return null;
			}
		});
	}

	/**
	 * @param response
	 * @param agent
	 * @throws SQLException
	 */
	public boolean checkAgentExists(String agent) {

		PreparedStatement pstmt = null;
		Connection connection = null;
		ResultSet sqlresult = null;
		try {
			connection = dataSource.getConnection();

			pstmt = connection.prepareStatement(CHECK_AGENT_EXISTS);
			pstmt.setString(1, agent);
			sqlresult = pstmt.executeQuery();
			return sqlresult.next();
		} catch (SQLException se) {
			throw new RuntimeException();
		} finally {
			if (sqlresult != null) {
				close(sqlresult);
			}
			if (pstmt != null) {
				close(pstmt);
			}
			if (connection != null) {
				close(connection);
			}
		}

	}

	/**
	 * @param response
	 * @param agentId
	 */
	public void deleteAgent(String agentId) {
		executePreparedStatement(new PreparedStatementTemplate<Object>() {
			@Override
			public PreparedStatement prepareStatement(Connection connection)
					throws SQLException {
				PreparedStatement pstmt = connection
						.prepareStatement(DELETE_AGENT);
				pstmt.setString(1, agentId);
				return pstmt;
			}

			@Override
			public Object prepareResult(int resultSet) throws SQLException {
				return null;
			}
		});
	}

	/**
	 * @param closeable
	 */
	private void close(AutoCloseable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			} catch (Exception e) {
			}
		}
	}

	/**
	 * @author Mohammad Qasem
	 *
	 */
	public interface PreparedStatementTemplate<T> {

		/**
		 * @param connection
		 * @return
		 * @throws SQLException
		 */
		PreparedStatement prepareStatement(Connection connection)
				throws SQLException;

		/**
		 * @param resultSet
		 * @return
		 * @throws SQLException
		 */
		T prepareResult(int resultSet) throws SQLException;

	}
}
