package com.telelogx.teletracker.license;

import java.io.Serializable;

import com.colureware.license.impl.ColurewareLicense;

/**
 * @author Mohammad Qasim
 *
 */
public class TeleTrackerLicenses extends ColurewareLicense implements
		Serializable, Cloneable {

	private static final long serialVersionUID = 1L;

	private int agentNumber;

	/**
	 * @return
	 */
	public int getAgentNumber() {
		return agentNumber;
	}

	/**
	 * @param agentNumber
	 */
	public void setAgentNumber(int agentNumber) {
		this.agentNumber = agentNumber;
	}

	public static void main(String[] args) {
		System.out.println(new TeleTrackerLicenses().getAgentNumber());
	}

}
