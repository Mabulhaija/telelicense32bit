/**
 * 
 */
package com.telelogx.teletracker.license;

/**
 * @author Mohammad Qasem
 * 
 *
 */
public class LicenseAPIException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    public LicenseAPIException() {
    }

    /**
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public LicenseAPIException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
	super(message, cause, enableSuppression, writableStackTrace);
    }

    /**
     * @param message
     * @param cause
     */
    public LicenseAPIException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * @param message
     */
    public LicenseAPIException(String message) {
	super(message);
    }

    /**
     * @param cause
     */
    public LicenseAPIException(Throwable cause) {
	super(cause);
    }

}
