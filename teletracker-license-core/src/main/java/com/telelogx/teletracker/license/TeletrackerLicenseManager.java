package com.telelogx.teletracker.license;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Base64Encoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.colureware.license.LicenseInstallationManager;
import com.colureware.license.LicenseVerificationManager;

/**
 * @author Mohammad Qasim
 *
 */
public class TeletrackerLicenseManager {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private LicenseInstallationManager licenseInstallationManager;
	@Autowired
	private LicenseVerificationManager licenseVerificationManager;

	private File licensefile;
	public TeleTrackerLicenses teleTrackerLicenses;

	/**
	 * @throws IOException
	 * 
	 */
	public void install() throws IOException {
		if (licensefile.exists()) {
			byte[] license = decodeLicense();
			licenseInstallationManager.installLicense(license);
		}
	}

	/**
	 * @return
	 * 
	 */

	public TeleTrackerLicenses verfyLecinces() {
		return teleTrackerLicenses = (TeleTrackerLicenses) licenseVerificationManager
				.verify();
	}

	/**
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private byte[] decodeLicense() throws IOException, FileNotFoundException {
		String encoded = IOUtils.toString(new FileInputStream(licensefile));
		Base64Encoder b64 = new Base64Encoder();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		b64.decode(encoded, baos);
		byte[] license = baos.toByteArray();
		return license;
	}

	/**
	 * @return the licensefile
	 */
	public File getLicensefile() {
		return licensefile;
	}

	/**
	 * @param licensefile
	 *            the licensefile to set
	 */
	public void setLicensefile(File licensefile) {
		this.licensefile = licensefile;
	}

	/**
	 * @return the teleTrackerLicenses
	 */
	public TeleTrackerLicenses getTeleTrackerLicenses() {
		return teleTrackerLicenses;
	}

	/**
	 * @param teleTrackerLicenses
	 *            the teleTrackerLicenses to set
	 */
	public void setTeleTrackerLicenses(TeleTrackerLicenses teleTrackerLicenses) {
		this.teleTrackerLicenses = teleTrackerLicenses;
	}
}
