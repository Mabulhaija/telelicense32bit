package com.telelogx.teletracker.license;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import sun.misc.BASE64Encoder;

/**
 * @author Mohammad Qasim
 *
 */
public class AgentIdDesEncryptor {

    private static final String ENC_KEY = "fe34ca11";
    private static final String DES = "DES/ECB/PKCS5Padding";

    private Cipher encryptionCipher;
    private SecretKey teletrackerKey;

    /**
     * @throws InvalidKeySpecException
     * 
     */
    public AgentIdDesEncryptor() {
	try {
	    byte[] keyBytes = ENC_KEY.getBytes("ASCII");
	    SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
	    teletrackerKey = factory.generateSecret(new DESKeySpec(keyBytes));

	    encryptionCipher = Cipher.getInstance(DES);
	    encryptionCipher.init(Cipher.ENCRYPT_MODE, teletrackerKey);

	} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeySpecException | InvalidKeyException
		| UnsupportedEncodingException e) {
	    throw new RuntimeException(e);
	}
    }

    public String Encrypt(String dateToEncypt) {
	try {
	    byte[] encodedDecryptedText = dateToEncypt.getBytes("UTF8");
	    byte[] EncrypedText = encryptionCipher.doFinal(encodedDecryptedText);
	    return new BASE64Encoder().encode(EncrypedText);
	} catch (IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException e) {
	    throw new RuntimeException(e);
	}
    }

    /**
     * @param args
     * @throws InvalidKeySpecException
     */
    public static void main(String[] args) throws InvalidKeySpecException {
	AgentIdDesEncryptor a = new AgentIdDesEncryptor();
	System.out.println(a.Encrypt("billingTeleTracker"));

    }
}
