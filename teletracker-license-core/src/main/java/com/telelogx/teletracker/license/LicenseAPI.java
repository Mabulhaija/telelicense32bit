/**
 * 
 */
package com.telelogx.teletracker.license;

import java.util.Set;

/**
 * @author Telelogx-pc
 *
 */
public interface LicenseAPI {

    int addAgentID(String agentId);

    int removeAgent(String agentID);

    Set<String> listAgents();

    void traceOperation(String operation, String agentID, String status, String description);

    String authorizationChecker(String agentID, String saltedAgent, String mixedDummyString);
}
