/**
 * 
 */
package com.telelogx.teletracker.license;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mohammad Qasem
 *
 */
public class LicenseAPIImpl implements LicenseAPI {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private TeletrackerLicenseDAO teletrackerLicenseDAO;
	private TeletrackerLicenseManager licenseManager;
	private AgentIdDesEncryptor desEncryptor = new AgentIdDesEncryptor();

	@Override
	public int addAgentID(String agentId) {
		Integer addstatus = 0;
		if (teletrackerLicenseDAO.checkAgentExists(agentId)) {

			traceOperation("ADD", agentId, "Failed", "agent Id already exists.");
			logger.error("agent Id already exists.");
			addstatus = 400;

		} else {
			TeleTrackerLicenses license = licenseManager
					.getTeleTrackerLicenses();
			if (license == null) {
				logger.error("No license installed.");
				traceOperation("ADD", agentId, "Failed",
						"No license installed.");
				addstatus = 401;

			}
			int maxNumberOfAgents = license.getAgentNumber();
			int cuurentAgents = teletrackerLicenseDAO.listAgents().size();
			if (cuurentAgents + 1 > maxNumberOfAgents) {
				logger.error("Max number of agents reached.");
				traceOperation("ADD", agentId, "Failed",
						"Max number of agents reached.");
				addstatus = 403;

			} else {
				addstatus = 200;
				teletrackerLicenseDAO.addAgent(agentId);
			}
		}
		return addstatus;
	}

	@Override
	public int removeAgent(String agentID) {
		int removeStatus = 0;
		if (!teletrackerLicenseDAO.checkAgentExists(agentID)) {
			logger.error("agent Id does not exists");
			traceOperation("Remove", agentID, "Failed",
					"agent Id does not exists");
			removeStatus = 400;

		} else {
			removeStatus = 200;
			teletrackerLicenseDAO.deleteAgent(agentID);
		}
		return removeStatus;

	}

	/**
	 * @return the teletrackerLicenseDAO
	 */
	public TeletrackerLicenseDAO getTeletrackerLicenseDAO() {
		return teletrackerLicenseDAO;
	}

	/**
	 * @param teletrackerLicenseDAO
	 *            the teletrackerLicenseDAO to set
	 */
	public void setTeletrackerLicenseDAO(
			TeletrackerLicenseDAO teletrackerLicenseDAO) {
		this.teletrackerLicenseDAO = teletrackerLicenseDAO;
	}

	/**
	 * @return the licenseManager
	 */
	public TeletrackerLicenseManager getLicenseManager() {
		return licenseManager;
	}

	/**
	 * @param licenseManager
	 *            the licenseManager to set
	 */
	public void setLicenseManager(TeletrackerLicenseManager licenseManager) {
		this.licenseManager = licenseManager;
	}

	@Override
	public Set<String> listAgents() {
		return teletrackerLicenseDAO.listAgents();

	}

	@Override
	public void traceOperation(String operation, String agentID, String status,
			String description) {
		teletrackerLicenseDAO.traceOperation(operation, agentID, status,
				description);

	}

	@Override
	public String authorizationChecker(String agentID, String saltedAgent,
			String mixedDummyString) {

		if (teletrackerLicenseDAO.checkAgentExists(agentID)) {
			teletrackerLicenseDAO.traceOperation("Authrization", agentID,
					"Success", "");
			return desEncryptor.Encrypt(saltedAgent);
		} else {
			teletrackerLicenseDAO.traceOperation("Authrization", agentID,
					"Failed", "this Agent Not Authrized");
			return desEncryptor.Encrypt(mixedDummyString);

		}
	}

}
