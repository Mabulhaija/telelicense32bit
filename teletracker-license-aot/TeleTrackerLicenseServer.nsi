!include ReplaceInFile.nsi
Name "TeleTracker"

OutFile "TeleTracker Server Installer.exe"
InstallDir $PROGRAMFILES

 Page directory
 Page instfiles
 UninstPage uninstConfirm
 UninstPage instfiles
 RequestExecutionLevel admin ;
Section "Installer Section"
SetOutPath "$INSTDIR\TeleTrackerLicense"
File /a /r  "TeletrackerLicenseWin-Service\"
Push {install_directory} 
Push "$INSTDIR\TeleTrackerLicense"
Push all
Push all 
Push "$INSTDIR\TeleTrackerLicense\setConfigurationArgument.bat"
Call ReplaceInFile

Push {install_directory} 
Push "$INSTDIR\TeleTrackerLicense"
Push all
Push all 
Push "$INSTDIR\TeleTrackerLicense\TeletrackerLicenseServiceInstaller.bat"
Call ReplaceInFile

Push {install_directory} 
Push "$INSTDIR\TeleTrackerLicense"
Push all
Push all 
Push "$INSTDIR\TeleTrackerLicense\startTeletrackerService.bat"
Call ReplaceInFile



CreateShortCut "$DESKTOP\TeletrackerLicenseWin-Service.lnk" "$INSTDIR\TeleTrackerLicense\TeletrackerLicenseWin-Service.exe"
ExecWait '"$INSTDIR\TeletrackerLicenseServiceInstaller.bat"' 
ExecWait '"$INSTDIR\setConfigurationArgument.bat"' 
SectionEnd

Section

WriteRegStr HKCU "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers"  "$INSTDIR\TeleTrackerLicense\TeletrackerLicenseWin-Service.exe"  "RUNASADMIN"

SectionEnd
